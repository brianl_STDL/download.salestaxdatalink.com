﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <img class="mb-4" src="i/Sales-Tax-Datalink-logo.png" alt="" height="100">
    <h1 runat="server" id="lblWelcome" class="h3 mb-3 font-weight-normal">Please sign in</h1>
    <asp:Label ID="lblError" CssClass="alert-danger" role="alert" runat="server" Text="" Visible="false" Style="padding: 2px 8px;"></asp:Label>
    <div class="form-group">
        <label for="inputEmail" class="sr-only">Email address</label>
        <asp:TextBox ID="tbEmail" type="email" class="form-control" placeholder="Email address" required autofocus runat="server"></asp:TextBox>
    </div>
    <div class="form-group">
        <label for="inputPassword" class="sr-only">Password</label>
        <asp:TextBox ID="tbPword" type="password" class="form-control" placeholder="Password" required runat="server"></asp:TextBox>
    </div>
    <div class="checkbox mb-3">
        <label>
            <%--<input type="checkbox" value="remember-me">--%>
            <asp:CheckBox ID="chkRemember" runat="server" value="remember-me" />
            Remember me
        </label>
        <p>
            <label>
                <a href="mailto:brianl@salestaxdatalink.com?Subject=I need help resetting my password">Contact support</a>
            </label>
        </p>
    </div>
    <asp:Button ID="btnSignIn" class="btn btn-lg btn-primary btn-block" type="submit" runat="server" Text="Sign In" OnClick="btnSignIn_Click" />
    <p class="mt-5 mb-3 text-muted">&copy; 2019</p>
    <script type="text/javascript">




</script>
</asp:Content>

