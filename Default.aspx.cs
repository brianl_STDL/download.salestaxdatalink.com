﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Request.QueryString["mode"] != null)
            {
                if (Request.QueryString["mode"].ToLower() == "newlogin")
                {
                    lblWelcome.InnerText = "Please sign in with your new password";
                }
            }
            //IF THE USER IS LOGGED IN, SEND THEM STRAIGHT TO THE DOWNLOADS PAGE.
            RedirectIfLoggedIn();
        }
    }

    /// <summary>
    /// THIS REDIRECT PREVENTS USERS FROM LANDING ON AN EMPTY DOWNLOADS PAGE. 
    /// SEND THEM TO THE LOGIN PAGE SO THEY CAN BE REDIRECTED TO THE DOWNLOADS PAGE.
    /// </summary>
    protected void RedirectIfLoggedIn()
    {
        var deleted = 1;
        string User_Name = string.Empty;
        HttpCookie reqCookies = Request.Cookies["userInfo"];
        if (reqCookies != null)
        {
            try
            {
                User_Name = reqCookies["UserName"].ToString();
            }
            catch (Exception)
            {
              
            }
        }

        if (User_Name != null && User_Name != "")
        {
            string FileAccess = "";
            //CHECK VALID USER
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["stdldbConnString"].ConnectionString);
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from [TaxRates].[dbo].[FileUser] where UserId =@username AND Deleted <> @deleted", con);
            cmd.Parameters.AddWithValue("@username", User_Name);
            cmd.Parameters.AddWithValue("@deleted", deleted);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                //UPDATE FILE ACCESS IN CASE OF DATABASE CHANGE
                foreach (DataRow row in dt.Rows)
                {
                    FileAccess = row["FileAccess"].ToString();
                }

                //SET A LOGIN COOKIE
                HttpCookie userInfo = new HttpCookie("userInfo");
                userInfo["UserName"] = User_Name;
                userInfo["FileArray"] = FileAccess;
                //userInfo.Expires.Add(new TimeSpan(43000, 1, 0));
                Response.Cookies.Add(userInfo);
                //USER IS LOGGED IN, SEND TO DOWNLOAD PAGE
                Response.Redirect("Download.aspx");
            }
            else
            {
               
            }
        }
    }

    /// <summary>
    /// WHEN SIGNING IN, USERS MAY BE USING A GENERIC PASSWORD STORED IN THE WEB.CONFIG FILE IN THE TEMPKEY SETTING.
    /// IF USER USES THE TEMP PASSWORD, THEN THEY ARE IMMEDIATELY REQUIRED TO ENTER A NEW MATCHING PASSWORD FOR THEIR LOGIN. 
    /// AFTER ENTERING A MATCHING PASSWORD, THE USER IS THEN REDIRECTED TO THE LOGIN PAGE WHERE THEY ARE ASKED TO ENTER THE NEW PASSWORD 
    /// THAT THEY JUST CREATED.
    /// ***********
    /// ALSO IN THIS METHOD WE ARE CREATING THE USER INFO COOKIE AND INCLUDING THE FILE NAMES IN THE COOKIE THAT THEY HAVE ACCESS TO.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSignIn_Click(object sender, EventArgs e)
    {
        string FileAccess = "";
        var tempPass = WebConfigurationManager.AppSettings["tempPass"];
        if (tbPword.Text.ToUpper() == tempPass.ToUpper())
        {
            //RESET PASSWORD BEFORE LOGGING IN
            Response.Redirect("ChangePass.aspx?email=" + tbEmail.Text);
        }
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["stdldbConnString"].ConnectionString);
        con.Open();
        //SqlCommand cmd = new SqlCommand("select * from [TaxRates].[dbo].[FileUser] where UserId =@username and NewPassword=@password", con);
        SqlCommand cmd = new SqlCommand("select * from [TaxRates].[dbo].[FileUser] where UserId =@username and NewPassword=(CAST(@password AS nvarchar(max)))", con);

        cmd.Parameters.AddWithValue("@username", tbEmail.Text);
        cmd.Parameters.AddWithValue("@password", tbPword.Text);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        da.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow row in dt.Rows)
            {
                FileAccess = row["FileAccess"].ToString();
            }

            //SET A LOGIN COOKIE
            HttpCookie userInfo = new HttpCookie("userInfo");
            userInfo["UserName"] = tbEmail.Text;
            userInfo["FileArray"] = FileAccess;
            //if (chkRemember.Checked == true)
            //{
            //    //SET USER TO STAY LOGGED IN 1 MONTH IF REMEMBER CHECKBOX IS TICKED
            userInfo.Expires.Add(new TimeSpan(43000, 0, 0));
            //}
            //else
            //{
            //    //CUT OFF LOGIN SESSION AFTER 1 HOUR
            //    userInfo.Expires.Add(new TimeSpan(1, 0, 0));
            //}
            Response.Cookies.Add(userInfo);
            //SEBD THE USER TO THE DOWNLOADS PAGE
            Response.Redirect("download.aspx");
        }
        else
        {
            //LOGIN FAILED VALIDATION
            lblError.Visible = true;
            lblError.Text = "Incorrect user name or password";
        }
    }

    //protected void KickUserOut()
    //{
    //    //KILL THE COOKIE AND KICK THE USER OUT
    //    if (Request.Cookies["userInfo"] != null)
    //    {
    //        HttpCookie myCookie = new HttpCookie("userInfo");
    //        myCookie.Expires = DateTime.Now.AddDays(-1d);
    //        Response.Cookies.Add(myCookie);
    //    }
    //    Session.Abandon();
    //    Session.Clear();
    //    Response.Redirect("Default.aspx");
    //}

}