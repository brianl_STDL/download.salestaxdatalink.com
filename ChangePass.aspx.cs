﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ChangePass : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["email"] == null)
        {
            Response.Redirect("Default.aspx");
        }
    }

    /// <summary>
    /// USER INPUTS MATCHING PASSWORDS INTO THE DATABASE. USUALLY AFTER INITALLY LOGIN WITH GENERIC PASSWORD 
    /// THAT A FIRST TIME USER WOULD USE.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSignIn_Click(object sender, EventArgs e)
    {
        try
        {
            string uname = "";
            //EMAIL IS REQUIRED IN THE URL WHEN RESETTING PASSWORD
            if (Request.QueryString["email"] != null)
            {
                uname = Request.QueryString["email"].ToString();
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["stdldbConnString"].ConnectionString);
                con.Open();
                SqlCommand cmd = new SqlCommand("UPDATE [TaxRates].[dbo].[FileUser] SET NewPassword = CAST(@password AS VARBINARY(255)) WHERE @username = @username", con);

                cmd.Parameters.AddWithValue("@username", uname);
                cmd.Parameters.AddWithValue("@password", password2.Text);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                cmd.ExecuteNonQuery();
                //REDIRECT TO LOGIN AGAIN WITH NEW PASSWORD
                //Response.Redirect("Default.aspx?mode=newlogin");
                lblWelcome.Visible = false;
                password1.Visible = false;
                password2.Visible = false;
                btnSignIn.Visible = false;
                lnkLoginAgain.Visible = true;
            }
            else
            {
                //IF NO EMAIL IS PRESENT IN THE URL, THEN THE USER HAS COME TO THIS PAGE BY MISTAKE. SEND THEM TO THE LOGIN PAGE.
                Response.Redirect("Default.aspx");
            }

        }
        catch (Exception exc)
        {
            lblErrorUpdatingPassword.Visible = true;
            Response.Redirect("Default.aspx");
        }
    }
}