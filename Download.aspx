﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Download.aspx.cs" Inherits="Download" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <img class="mb-4" src="i/Sales-Tax-Datalink-logo.png" alt="" height="100">
    <p>
        <asp:Label ID="lblWelcome" runat="server" Text=""></asp:Label></p>
    <h3 class="h3 mb-3 font-weight-normal">Your Available Downloads</h3>
    <table id="linkTable" class="table table-hover">
        <thead>
            <tr>
                <th scope="col">File</th>
                <th scope="col">Link</th>

            </tr>
        </thead>
        <tbody>
            <asp:Literal ID="litDownloadLinks" runat="server"></asp:Literal>
        </tbody>
    </table>
    <p>
        <asp:Button ID="btnLogOut" class="btn btn-lg btn-primary btn-sm btn-block" type="submit" runat="server" Text="Logout" OnClick="btnLogOut_Click" />
    </p>

    <script>
        var elements = document.querySelectorAll("[id='linkTable']");
        for (var i = 0; i < elements.length; i++) {
            
  //if (elements[i].getAttribute("href") === "www.dropbox.com") {
   // elements[i].style.display='none';
 // }
}
</script>
</asp:Content>

