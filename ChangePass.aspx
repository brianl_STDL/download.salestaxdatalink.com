﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ChangePass.aspx.cs" Inherits="ChangePass" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <img class="mb-4" src="i/Sales-Tax-Datalink-logo.png" alt="" height="100">

    <%--<h1 class="h3 mb-3 font-weight-normal">Please update your password</h1>--%>
    <h1><asp:Label ID="lblWelcome" runat="server"  class="h3 mb-3 font-weight-normal" Text="Please update your password"></asp:Label></h1>
    <div class="form-group">
        <label for="password1" class="sr-only">Password</label>
        <asp:TextBox ID="password1" type="password" class="form-control" placeholder="Password" required runat="server"></asp:TextBox>
    </div>
    <div class="form-group">
        <label for="password2" class="sr-only">Repeat Password</label>
        <asp:TextBox runat="server" ID="password2" type="password" class="form-control" data-match="#password1" data-match-error="Whoops, these don't match" placeholder="Confirm Pasword" required></asp:TextBox>
   <div id="passvalidate" class="validate alert alert-success" role="alert" style="display: none;"></div> 
    <div id="failvalidate" class="validate alert alert-danger" role="alert" style="display: none;"></div>
 

    </div>
    <asp:Button ID="btnSignIn" class="btn btn-lg btn-primary btn-block"  data-toggle="button" UseSubmitBehavior="False" type="submit" runat="server" Text="Submit New Password" OnClick="btnSignIn_Click" />
    <asp:Label ID="lblErrorUpdatingPassword"  class="validate alert alert-danger" Visible="false" runat="server" Text="An error occurred while updating your password. Please try again." ></asp:Label>
    <asp:LinkButton ID="lnkLoginAgain" Visible="false" runat="server" PostBackUrl="~/Default.aspx?mode=newlogin">Password Successfully changed. Please login again.</asp:LinkButton>
    <script type="text/javascript">
        $(document).ready(function () {           
            $("#ContentPlaceHolder1_btnSignIn").prop('disabled', true);
            $("#ContentPlaceHolder1_password2").keyup(validate);
        });
         
        function validate() {
            var password1 = $("#ContentPlaceHolder1_password1").val();
            var password2 = $("#ContentPlaceHolder1_password2").val();

            if (password1 == password2) {
                $(".validate").hide();
                $("#passvalidate").show();
                $("#passvalidate").text("passwords match");
                $("#ContentPlaceHolder1_btnSignIn").prop('disabled', false);
                $("#ContentPlaceHolder1_btnSignIn").focus();

            }
            else {
                $(".validate").hide();
                $("#failvalidate").show();
                $("#failvalidate").text("passwords do not match");
                $("#ContentPlaceHolder1_btnSignIn").prop('disabled', true);
                
            }
        }
                   
    </script>
 </asp:Content>

