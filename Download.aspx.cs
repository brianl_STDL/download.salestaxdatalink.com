﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

public partial class Download : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //CHECK USER LOGGED IN
            if (Request.Cookies["userInfo"] != null)
            {
                //CHECK CREDENTIALS
                SecureLoginCheck();
            }
            else
            {
                KickUserOut();
            }
        }       
    }
    /// <summary>
    /// KILL THE LOGGED IN USER AND REDIRECT USER TO LOG IN PAGE
    /// </summary>
    protected void btnLogOut_Click(object sender, EventArgs e)
    {
        if (Request.Cookies["userInfo"] != null)
        {
            HttpCookie myCookie = new HttpCookie("userInfo");
            myCookie.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(myCookie);
        }
        Session.Abandon();
        Session.Clear();
        Response.Redirect("Default.aspx");
    }

    /// <summary>
    /// WE MUST CHECK THAT THE COOKIE ACTUALLY CONTAINS A USER NAME IN THE SYSTEM AND THAT THE USER IS ACTIVE.
    /// IF USER IS ACTIVE AND LOGGED IN, WE LOG THE DATE AND TIME OF USER LOG IN.
    /// </summary>
    protected void SecureLoginCheck()
    {
        var deleted = 1;
        string User_Name = string.Empty;
        HttpCookie reqCookies = Request.Cookies["userInfo"];
        if (reqCookies != null)
        {
            try
            {         
            User_Name = reqCookies["UserName"].ToString();
            }
            catch (Exception)
            {
                KickUserOut();
            }
        }

        if (User_Name != null && User_Name != "")
        {
            //WELCOME USER
            lblWelcome.Text = "Welcome, " + User_Name.ToString();
            string FileAccess = "";
            //CHECK VALID USER
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["stdldbConnString"].ConnectionString);
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from [TaxRates].[dbo].[FileUser] where UserId =@username AND Deleted <> @deleted", con);
            cmd.Parameters.AddWithValue("@username", User_Name);
            cmd.Parameters.AddWithValue("@deleted", deleted);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                //UPDATE FILE ACCESS IN CASE OF DATABASE CHANGE
                foreach (DataRow row in dt.Rows)
                {
                    FileAccess = row["FileAccess"].ToString();
                }

                //SET A LOGIN COOKIE
                HttpCookie userInfo = new HttpCookie("userInfo");
                userInfo["UserName"] = User_Name;
                userInfo["FileArray"] = FileAccess;
                //userInfo.Expires.Add(new TimeSpan(43000, 1, 0));
                Response.Cookies.Add(userInfo);

                //BUILD DOWNLOAD LINKS
                BuildDownloadLinks();
                //UPDATED TABLE FOR LOGGEDIN INFO
                LogUserActivity(User_Name);
            }
            else
            {
                KickUserOut();
            }
        }
    }

    /// <summary>
    /// RECORD DATE/TIME OF USER LAST LOGIN IN DATABASE
    /// </summary>
    /// <param name="User_Name"></param>
    protected void LogUserActivity(string User_Name)
    {
        try
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["stdldbConnString"].ConnectionString);
            con.Open();
            SqlCommand cmd = new SqlCommand("UPDATE [TaxRates].[dbo].[FileUser] SET [LastLoginDateTime] = GETDATE() WHERE Userid = @username", con);

            cmd.Parameters.AddWithValue("@username", User_Name);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            cmd.ExecuteNonQuery();
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <summary>
    /// FILE NAMES ARE STORED IN THE DATABASE AS COMMA SEPARATED VALUES.
    /// WE WILL RETRIEVE THESE VALUES FROM THE COOKIE AND LOOP THROUGH THEM TO START BUILDING LINKS TO DOWNLOAD FILES
    /// </summary>
    protected void BuildDownloadLinks()
    {
        string table = "";
        string FileAccess = string.Empty;
        HttpCookie reqCookies = Request.Cookies["userInfo"];
        if (reqCookies != null)
        {
            try
            {
                FileAccess = reqCookies["FileArray"].ToString();
            }
            catch (Exception)
            {

                KickUserOut();
            }

            //IF THE VARIABLE CONTAINS A COMMMA, THEN IT IS A COMMA SEPARATED VALUE. WE NEED TO SPLIT IT INTO AN ARRAY.
            if (FileAccess.Contains(','))
            {
                //SPLIT THE STRING AND LOOP
                string[] files = FileAccess.Split(',');
                foreach (string file in files)
                {
                    //LOOP THROUGH RECORDS TO CREATE A TABLE OF DOWNLOADABLE LINKS WIHT DESCRIPTIONS
                     table += "<tr><th scope='row'>" + file + "</th><td> <a class='button' href='" + GetFileLinks(file) + "'>DOWNLOAD</a></td></tr>";
                }
            }
            else
            {
                //IN THIS CASE, ONLY ONE FILE IS RETURNED FOR THE USER, SO ONLY PRESENT ONE LINK
                table += "<tr><th scope='row'>" + FileAccess + "</th><td> <a class='button' href='" + GetFileLinks(FileAccess) + "'>DOWNLOAD</a></td></tr>";
            }

            //DISPLAY THE TABLE
            litDownloadLinks.Text = table;
        }
        else
        {
            //IF WE GET BACK A NULL VALUE FOR FILE ACCESS, KICK THE USER OUT
            KickUserOut();
        }
    }

    /// <summary>
    /// GET DROP BOX LINKS FOR FILES
    /// </summary>
    /// <param name="file"></param>
    protected string GetFileLinks(string file)
    {
        string link = "";

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["stdldbConnString"].ConnectionString);
        con.Open();
        SqlCommand cmd = new SqlCommand("SELECT [FilePath] FROM [TaxRates].[dbo].[FileXREF] WHERE FileAccess = @fileaccess", con);
        cmd.Parameters.AddWithValue("@fileaccess", file);
        SqlDataReader reader = cmd.ExecuteReader();

        if (reader.HasRows)
        {
            while (reader.Read())
            {
                link = reader["FilePath"].ToString();
            }
        }
        else
        {
            link = "#";
        }
        reader.Close();

        return link;
    }

    /// <summary>
    /// KILL THE SESSION AND COOKIE AND KICK THE USER BACK TO THE LOGIN PAGE. 
    /// THIS IS USED ANYTIME AN ERROR OCCURS WHEN CHECKING FOR VALID USER.
    /// </summary>
    protected void KickUserOut()
    {
        //KILL THE COOKIE AND KICK THE USER OUT
        if (Request.Cookies["userInfo"] != null)
        {
            HttpCookie myCookie = new HttpCookie("userInfo");
            myCookie.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(myCookie);
        }
        Session.Abandon();
        Session.Clear();
        Response.Redirect("Default.aspx");
    }
}